Sector Shop

Commerce feature and supporting functionality - optional (contrib) addition for Sector.

Installation:
Download sector according to the instructions on the sector page.

Download this project and copy the included makefile (build-sector-shop.make) into the sector directory

In your custom project make file add the following line:

includes[] = build-sector-shop.make
Build sector, eg by running the following command

drush make build-sector.make ../builds/sector
(change the build directory as appropriate)

OR if adding after build download and install this feature (and its dependencies) as normal, eg via drush.

After the site is built and database installed enable the 'Sector shop' feature either on the features admin page or modules page under 'Sector optional features'.

Usage:
This module provides the necessary commerce dependencies, features, and configuration for a basic shop.

Supporting organizations: 
Sparks Interactive
