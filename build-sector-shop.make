defaults[projects][subdir] = "contrib"

; Sector shop
projects[sector_shop][type] = module
projects[sector_shop][download][type] = git
projects[sector_shop][download][url] = https://git.drupal.org/sandbox/somatick/2902760.git
projects[sector_shop][download][branch] = 7.x-1.x

; Commerce dependencies
projects[rules][version] = "2.10"
projects[addressfield][version] = "1.2"

; Commerce
projects[commerce][version] = "1.13"
; Fix 'Only variables should be passed by reference' Notice. See https://www.drupal.org/node/2762907
projects[commerce][patch][] = "https://www.drupal.org/files/issues/commerce-cart_product_add_php7-2762907-5.patch"

; Commerce contrib dependencies
projects[physical][version] = "1.0"
projects[inline_conditions][version] = "1.0-alpha7"
projects[eva][version] = "1.3"
projects[inline_entity_form][version] = "1.8"
projects[views_megarow][version] = "1.7"

; Commerce contrib 
projects[commerce_features][version] = "1.2"
projects[commerce_backoffice][version] = "1.5"
projects[commerce_coupon][version] = "2.0"
projects[commerce_discount][version] = "1.0-alpha8"
projects[commerce_custom_product][version] = "1.0-beta2"
projects[commerce_paypal][version] = "2.4"
projects[commerce_braintree][version] = "2.0"
projects[commerce_dps][version] = "1.1"
projects[commerce_flo2cash][version] = "1.0"
projects[commerce_shipping][version] = "2.2"
projects[commerce_flat_rate][version] = "1.0-beta2"
projects[commerce_shipping_weight_tariff][version] = "1.0-beta3"
projects[commerce_popup_cart][version] = "1.0"


; Dependency with no release
projects[commerce_physical][version] = "1.x-dev"
projects[commerce_physical][download][type] = "git"
projects[commerce_physical][download][url] = "http://git.drupal.org/project/commerce_physical.git"
projects[commerce_physical][download][revision] = "477aaeee767416fe3b0ebee023d1cbb3a3434718"


; Libraries
libraries[braintree][download][type] = "get"
libraries[braintree][download][url] = "https://github.com/braintree/braintree_php/archive/3.21.0.tar.gz"
libraries[braintree][directory_name] = "braintree_php"
libraries[braintree][type] = "library"




