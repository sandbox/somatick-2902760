<?php
/**
 * @file
 * sector_product.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function sector_product_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer product display types'.
  $permissions['administer product display types'] = array(
    'name' => 'administer product display types',
    'roles' => array(
      'Drupal site builder (site architects and developers only)' => 'Drupal site builder (site architects and developers only)',
    ),
    'module' => 'commerce_backoffice_product',
  );

  // Exported permission: 'administer weight tariff services'.
  $permissions['administer weight tariff services'] = array(
    'name' => 'administer weight tariff services',
    'roles' => array(
      'Drupal site builder (site architects and developers only)' => 'Drupal site builder (site architects and developers only)',
    ),
    'module' => 'commerce_shipping_weight_tariff',
  );

  // Exported permission: 'configure store'.
  $permissions['configure store'] = array(
    'name' => 'configure store',
    'roles' => array(
      'Drupal site builder (site architects and developers only)' => 'Drupal site builder (site architects and developers only)',
    ),
    'module' => 'commerce',
  );

  return $permissions;
}
