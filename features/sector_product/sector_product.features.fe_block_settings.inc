<?php
/**
 * @file
 * sector_product.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function sector_product_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['commerce_cart-cart'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'cart',
    'module' => 'commerce_cart',
    'node_types' => array(),
    'pages' => 'checkout*',
    'roles' => array(),
    'themes' => array(
      'sector_starter' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sector_starter',
        'weight' => 0,
      ),
      'shiny' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'shiny',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['commerce_shipping_weight_tariff-shipping_matrix'] = array(
    'cache' => 8,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'shipping_matrix',
    'module' => 'commerce_shipping_weight_tariff',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'sector_starter' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sector_starter',
        'weight' => 0,
      ),
      'shiny' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'shiny',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
